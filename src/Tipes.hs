module Tipes where

import           Lib.Prelude
import           Model

data IsiOmongan
  = ProperData Omah
  | CariOmah
  | TambahOmah
  | TambahHanyaJudul Text
  | TambahJudulPlusProv Text Text
  | TambahJudulProvPlusKota Text Text Text
  | CariHanyaProv Text
  | CariProvPlusKota Text Text
  | CariProvKotaDanHarga Text Text Double
  | Sepi
  deriving Show

data ModelOmah = ModelOmah
  { currentData :: IsiOmongan
  } deriving Show

initModelOmah :: ModelOmah
initModelOmah = ModelOmah {currentData = Sepi}

data Aksi
  = Mandeg
  | TulunginGuwe
  | TambahTeks Text
  | TambahHarga Double
  | AksiCari
  | AksiTambah
  deriving (Show, Read)

