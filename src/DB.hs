{-# LANGUAGE AllowAmbiguousTypes   #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE Rank2Types            #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeFamilies          #-}
module DB where

import           Lib.Prelude                 hiding (from)
import           Model

import           Control.Monad.Logger
import           Control.Monad.Trans.Control
import           Database.Esqueleto
import           Database.Persist.Postgresql (createPostgresqlPool,
                                              withPostgresqlConn)

runQuery :: ReaderT SqlBackend (LoggingT IO) b -> IO b
runQuery query = do
  let con = "host=localhost port=5432 user=ibnu dbname=bot password=jaran"
  runStderrLoggingT $ withPostgresqlConn con $ \backend -> runReaderT query backend

something :: (MonadIO m, MonadBaseControl IO m, MonadLogger m) => m ()
something = do
  let con = "host=localhost port=5432 user=ibnu dbname=bot password=jaran"
  pool <- createPostgresqlPool con 10
  liftIO $ runSqlPool doMigration pool

insertOmah :: Omah -> IO (Key Omah)
insertOmah omah = runQuery $ insert omah

searchOmahWithCriteria :: Text -> Text -> Double -> IO [Omah]
searchOmahWithCriteria provinsi kota harga = do
  omahOmah <-
    runQuery $
    select $
    from $ \omah -> do
      where_
        ((omah ^. OmahHarga) <=. (val harga) &&. (omah ^. OmahProvinsi) ==. (val provinsi) &&.
         (omah ^. OmahKota) ==.
         (val kota))
      limit 5
      return omah
  return $ map entityVal omahOmah
