module Lib
  ( lariLari
  ) where

import           DB
import           Lib.Prelude
import           Model
import           Tipes

import           Control.Monad.Logger
import           Data.Text
import           System.Environment
import           Text.Read                        (readMaybe)

import           Telegram.Bot.API
import           Telegram.Bot.Simple
import           Telegram.Bot.Simple.UpdateParser

omahOmahBot :: BotApp ModelOmah Aksi
omahOmahBot = BotApp
  { botInitialModel = initModelOmah
  , botAction = flip updateJadiAksi
  , botHandler = handleAksi
  , botJobs = []
  }

pesanPesanBerikutIni :: Text
pesanPesanBerikutIni =
  unlines
    [ "Silakan pilih antara"
    , "/help untuk menampilkan pesan ini."
    , "/cari untuk mencari."
    , "/tambah untuk menambah."
    ]

updateJadiAksi :: ModelOmah -> Update -> Maybe Aksi
updateJadiAksi _ =
  parseUpdate $
  AksiCari       <$  command "cari" <|>
  TulunginGuwe   <$  command "help" <|>
  AksiTambah     <$  command "tambah" <|>
  TambahTeks     <$> plainText <|>
  callbackQueryDataRead

handleAksi :: Aksi -> ModelOmah -> Eff Aksi ModelOmah
handleAksi aksi omahomah =
  case aksi of
    Mandeg -> pure omahomah
    TulunginGuwe ->
      omahomah <# do
        reply (toReplyMessage pesanPesanBerikutIni)
        return Mandeg
    AksiTambah ->
      tambahMulai omahomah <# do
        reply "judul iklan apaan bray?"
        return Mandeg
    AksiCari ->
      cariMulai omahomah <# do
        reply "provinsi mana bray?"
        return Mandeg
    TambahTeks teks ->
      case omahomah of
        ModelOmah {currentData = CariOmah} ->
          ModelOmah {currentData = CariHanyaProv teks} <# do
            reply "Kalau kotanya?"
            return Mandeg
        ModelOmah {currentData = CariHanyaProv prov} ->
          ModelOmah {currentData = CariProvPlusKota prov teks} <# do
            reply
              (toReplyMessage "Kalau harganya diatas:")
              {replyMessageReplyMarkup = Just $ SomeReplyKeyboardMarkup pilihanHargaDiKibor}
            return Mandeg
        ModelOmah {currentData = CariProvPlusKota prov kota} ->
          case (readMaybe $ unpack teks :: Maybe Double) of
            Nothing ->
              ModelOmah {currentData = CariProvPlusKota prov kota} <# do
                reply "Mohon isi dengan angka saja."
                return Mandeg
            Just d ->
              ModelOmah {currentData = CariProvKotaDanHarga prov kota d} <# do
                omahBanyak <- liftIO $ searchOmahWithCriteria prov kota d
                mapM_ (reply . toReplyMessage . show) omahBanyak
                return Mandeg
        ModelOmah {currentData = TambahOmah} ->
          ModelOmah {currentData = TambahHanyaJudul teks} <# do
            reply "Mau jual di provinsi mana, bray?"
            return Mandeg
        ModelOmah {currentData = TambahHanyaJudul judul} ->
          ModelOmah {currentData = TambahJudulPlusProv judul teks} <# do
            reply "Mau jual di kota mana, bray?"
            return Mandeg
        ModelOmah {currentData = TambahJudulPlusProv judul prov} ->
          ModelOmah {currentData = TambahJudulProvPlusKota judul prov teks} <# do
            reply
              (toReplyMessage "Kalau harganya diatas:")
              {replyMessageReplyMarkup = Just $ SomeReplyKeyboardMarkup pilihanHargaDiKibor}
            return Mandeg
        ModelOmah {currentData = TambahJudulProvPlusKota judul prov kota} ->
          case (readMaybe $ unpack teks :: Maybe Double) of
            Nothing ->
              ModelOmah {currentData = TambahJudulProvPlusKota judul prov kota} <# do
                reply "mohon hanya angka saja."
                return Mandeg
            Just d ->
              ModelOmah {currentData = ProperData $ Omah judul prov kota d} <# do
                let omah = Omah judul prov kota d
                    modelOmah = ModelOmah {currentData = ProperData omah}
                _ <- liftIO $ insertOmah omah
                reply . toReplyMessage . show $ modelOmah
                return Mandeg
        omahh ->
          omahomah {currentData = Sepi} <# do
            reply . toReplyMessage . show $ omahh
            reply (toReplyMessage pesanPesanBerikutIni)
            return Mandeg
    _ ->
      omahomah <# do
        reply (toReplyMessage pesanPesanBerikutIni)
        return Mandeg


pilihanHargaDiKibor :: ReplyKeyboardMarkup
pilihanHargaDiKibor =
  ReplyKeyboardMarkup
  { replyKeyboardMarkupKeyboard =
      [ ["200000000"]
      , ["400000000"]
      , ["800000000"]
      , ["1000000000"]
      , ["1200000000"]
      , ["1400000000"]
      ]
  , replyKeyboardMarkupResizeKeyboard = Just True
  , replyKeyboardMarkupOneTimeKeyboard = Just True
  , replyKeyboardMarkupSelective = Nothing
  }

tambahMulai :: ModelOmah -> ModelOmah
tambahMulai ModelOmah {currentData = Sepi} = ModelOmah {currentData = TambahOmah}
tambahMulai omahomah = omahomah

cariMulai :: ModelOmah -> ModelOmah
cariMulai ModelOmah {currentData = Sepi} = ModelOmah {currentData = CariOmah}
cariMulai omahomah                       = omahomah

lari :: Token -> IO ()
lari token = do
  env <- defaultTelegramClientEnv token
  startBot_ (conversationBot updateChatId omahOmahBot) env

lariLari :: IO ()
lariLari = do
  runStderrLoggingT something
  token <- Token . pack <$> getEnv "TELEGRAM_TOKEN"
  lari token
